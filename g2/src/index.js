import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { HashRouter, Switch, Route, Redirect } from 'react-router-dom'
import { isAuthenticated } from './services/auth'
import Home from './pages/Home';

const PrivateRoute = ({ component: Component }) => (
	<Route
		render={props => isAuthenticated() === true ? (
			<Component {...props} />
		) : (
				<Redirect to={{ pathname: "/", state: { from: props.location } }} />
			)}
	/>
)

ReactDOM.render(
	<HashRouter>
		<Switch>
			<Route path="/" exact={true} component={App} />
			<PrivateRoute path="/home" component={Home} />
			<Route path='*' component={App} />
		</Switch>
	</HashRouter>
	, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
