import firebase from 'firebase'

var firebaseConfig = {
	apiKey: "AIzaSyD9mFP1nByXaBC3j8xrFEPePnuBmp5Gyg0",
	authDomain: "projeto-react-6eee8.firebaseapp.com",
	databaseURL: "https://projeto-react-6eee8.firebaseio.com",
	projectId: "projeto-react-6eee8",
	storageBucket: "projeto-react-6eee8.appspot.com",
	messagingSenderId: "249442150324",
	appId: "1:249442150324:web:51a5a6b4742a4d5950f4e1"
};

firebase.initializeApp(firebaseConfig);

export const isAuthenticated = () => {
	return sessionStorage.getItem("login") !== null;
}

export const login = (email, password) => {
	return new Promise((resolve, reject) => {
		firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then(() => {
				sessionStorage.setItem("login", true)
				resolve()
			})
			.catch((erro) => {
				reject(erro)
			});
	});
}

export const signUp = (email, password) => {
	return new Promise((resolve, reject) => {
		firebase
			.auth()
			.createUserWithEmailAndPassword(email, password)
			.then(() => {
				resolve("Usuário criado com sucesso!")
			})
			.catch(error => {
				reject(error)
			});
	});
}

export const logoff = () => {
	sessionStorage.removeItem("login")
	return new Promise((resolve, reject) => {
		firebase.auth().signOut()
			.then(() => resolve());
	});
}