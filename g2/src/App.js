import React, { Component } from 'react'
import { login, signUp } from './services/auth'

import './assets/css/style.css';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errorMessage: "",
      successMessage: "",
      loading: false
    }
  }

  logon = async() => {
    this.setState({ loading: true })
    await login(this.state.email, this.state.password)
      .then(() => this.props.history.push("/home"))
      .catch(erro => this.setState({ errorMessage: erro.message }))
  }

  salvarUsuario = async () => {
    this.setState({ loading: true })
    await signUp(this.state.email, this.state.password)
      .then(msg => this.setState({ successMessage: msg, errorMessage: "" }))
      .catch(msg => this.setState({ errorMessage: msg.message, successMessage: "" }))
    this.setState({ loading: false })
  }

  render() {
    return (
      <div className="App">
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100">
              <div className="login100-form validate-form">
                <span className="login100-form-title p-b-26">
                  Bem Vindo!
              </span>
                <span className="login100-form-title p-b-48">
                  <i className="zmdi zmdi-font" />
                </span>
                <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                  <input className="input100" type="text" name="email" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value }) }/>
                  <span className="focus-input100" data-placeholder="Email" />
                </div>
                <div className="wrap-input100 validate-input" data-validate="Enter password">
                  <span className="btn-show-pass">
                    <i className="zmdi zmdi-eye" />
                  </span>
                  <input className="input100" type="password" name="pass" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value }) }/>
                  <span className="focus-input100" data-placeholder="Senha" />
                </div>
                <div className="container-login100-form-btn">
                  <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn" />
                    <button className="login100-form-btn" onClick={() => this.logon()}>
                      Login
                  </button>
                  </div>
                </div>

                <div className="container-login100-form-btn">
                  <div className="wrap-login100-form-btn">
                    <div className="login100-form-bgbtn" />
                    <button className="login100-form-btn" onClick={() => this.salvarUsuario()}>
                      Registre-se
                  </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
